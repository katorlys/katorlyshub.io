# katorlys.github.io
[![License](https://img.shields.io/badge/license-GPLv3-blue?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.html) [![Pull Requests](https://img.shields.io/github/issues-pr-closed/katorlys/katorlys.github.io?style=flat-square)](https://github.com/katorlys/katorlys.github.io/pulls) [![Issues](https://img.shields.io/github/issues-closed/katorlys/katorlys.github.io?style=flat-square)](https://github.com/katorlys/katorlys.github.io/issues) [![Lines](https://img.shields.io/tokei/lines/github/katorlys/katorlys.github.io?style=flat-square)](https://github.com/katorlys/katorlys.github.io)

## Introduction
Katorly Lab website hosted at [https://katorlys.github.io](https://katorlys.github.io) and [https://katorly.gitee.io/lab](https://katorly.gitee.io/lab)  
Do not use, copy, modify, or distribute.  

Theme by Katorly.  

## Why used Gitee?
[github.io](https://github.io) has been blocked by the great firewall of China since 2013, so that Chinese users can't visit it freely. By using Gitee, the website can be visited globally.